import sys
import os
from shutil import move
import json
import re

if len(sys.argv) != 2:
    print('enter corpus folder path as argument')
    exit()

def clean(filename):
    #Create temp file
    with open(filename+'_mdf', 'wb') as new_file:
        with open(filename, encoding="utf-8") as old_file:
            for line in old_file:
                document = json.loads(line)
                document['text'] = re.sub('[_\*%/\–\-\[\]?!:.,\n()\"]', ' ', document['text']).lower()
                document['text'] = re.sub('\d+', 'cislovka', document['text'])
                new_file.write(json.dumps(document, ensure_ascii=False).encode('utf8'))
                new_file.write(b'\n')
    #Remove original file
    os.remove(filename)
    #Move new file
    move(filename+'_mdf', filename)

for root, dirs, files in os.walk(sys.argv[1]):  
    for filename in files:
        clean(os.path.join(root, filename))