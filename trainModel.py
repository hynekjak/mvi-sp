import sys
from gensim.models.ldamulticore import LdaMulticore
from gensim.corpora.dictionary import Dictionary
from os import path
import os
import json

if len(sys.argv) != 2:
    print('enter corpus folder path as argument')
    exit()

#nactu slovnik ze souboru
temp_file = path.join(path.dirname(path.abspath(__file__)), "model", "dictionary" )
common_dictionary = Dictionary.load(temp_file)
print('slovnik nacten')

# inicializuji prazdny model
lda = LdaMulticore( num_topics=10, workers=3, id2word=common_dictionary )
print('model inicializovan')

for root, dirs, files in os.walk(sys.argv[1]):
    for dir in dirs:
        common_texts = list()
        for r, d, f in os.walk(path.join(root,dir)):
            for file in f:
                with open(path.join(r,file), 'r') as wikiDump:
                    for k in wikiDump:
                        document = json.loads(k)
                        common_texts.append(document['text'].split())
        common_corpus = [common_dictionary.doc2bow(text) for text in common_texts]
        common_corpus = list(filter(lambda x: len(x) > 0, common_corpus))
        print('korpus vytvoren')
        lda.update(common_corpus)
        print('slozka '+path.join(root,dir)+' natrenovana')
    break

# Ulozim model do souboru
temp_file = path.join(path.dirname(path.abspath(__file__)), "model", "trained_model" )
lda.save(temp_file)

