# Semestrální práce MI-MVI v LS 18/19
## Zadání
Vytvořte systém, který pro celý článek spočítá vektor, který bude reprezentovat jeho obsah. Můžete si vybrat, jestli budete modelovat články v češtině nebo angličtině.

## Postup
Budu pracovat s články v českém jazyce. Použiji data z české wikipedie, na kterých naučím [lda2vec](https://radimrehurek.com/gensim/models/ldamodel.html). Následně budu ručně náhodně kontrolovat kvalitu výsledků vyhledáním podobných článků k zadanému článku a porovnáním obsahu.

## Prostředí
Semestrální práce je realizována v pythonu.
Pro rozběhnutí semestrální práce je potřeba nainstalovat modul gensim
```
pip install --upgrade gensim
```
je také nutné použít [wikiextractor](https://github.com/attardi/wikiextractor).

## Stažení dat
[Dump článků české wikipedie](https://dumps.wikimedia.org/cswiki/latest/cswiki-latest-pages-articles.xml.bz2)  
Stažená data je potřeba vyextrahovat do jednotlivých dokumentů pomocí [wikiextractor](https://github.com/attardi/wikiextractor). 
Data je potřeba vyexportovat ve formátu json příkazem:
```
./wikiextractor/WikiExtractor.py --json --processes 4 -q cswiki-latest-pages-articles.xml
```
Wikiextractor vytvoří složku obsahující podsložky AA, AB, .. obsahující soubory s JSON dokumenty. 
Následně je třeba spustit skript [cleanSymbolsFromCorpus.py](cleanSymbolsFromCorpus.py), který jako parametr přijímá cestu ke složce vytvořené wikiextractorem. Pro práci a testování modelu jsem data z wikiextractoru rozdělil do dvou složek, na trénovací a testovací data, pro testování jsem použil poslední složku (AH) očištěných dat.

## Trénování a testování modelu
Trénování modelu je rozděleno do několika kroků pro možné opakování dílčích výpočtů.
  * Je potřeba vytvořit slovník pomocí skriptu [createDictionary.py](createDictionary.py), který jako parametr 
přijímá cestu ke složce s libovolnou podadresářovou strukturou, ve které jsou datové soubory wikiextraktoru. 
Slovník se uloží do složky s názvem model.
  * Dále je nutné natrénovat model skriptem [trainModel.py](trainModel.py), který jako parametr přijímá cestu 
ke složce s daty, v tomto případě ke stejné složce z předchozího kroku. Natrénovaný model se uloží do 
složky s názvem model.
  * Skriptem [printTopics.py](printTopics.py) je možné vypsat témata naučené v modelu. Skript načítá slovník a 
model ze složky s názvem model.
  * S vytvořeným slovníkem a natrénovaným modelem je možné testovat model skriptem [testModel.py](testModel.py), 
který jako parametr přijímá cestu ke složce s testovacími daty. Jako výsledek vytvoří skript ve složce 
test_results textový soubor pro každé téma. V každém souboru jsou vypsané články klasifikovány pro toto téma.
