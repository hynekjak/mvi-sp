import sys
from gensim.corpora.dictionary import Dictionary
import os
import json

if len(sys.argv) != 2:
    print('enter corpus folder path as argument')
    exit()

common_texts = list()

for root, dirs, files in os.walk(sys.argv[1]):  
    for filename in files:
        with open(os.path.join(root, filename), 'r') as wikiDump:
            for k in wikiDump:
                document = json.loads(k)
                common_texts.append(document['text'].split())

common_dictionary = Dictionary(common_texts)

# no_below (int, optional) – Keep tokens which are contained in at least no_below documents.
# no_above (float, optional) – Keep tokens which are contained in no more than no_above documents (fraction of total corpus size, not an absolute number).
# keep_n (int, optional) – Keep only the first keep_n most frequent tokens.
# keep_tokens (iterable of str) – Iterable of tokens that must stay in dictionary after filtering.
common_dictionary.filter_extremes(no_below=3, no_above=0.4, keep_n=200000, keep_tokens=None)

# save the dictionary in folder ./model
temp_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), "model", "dictionary" )
common_dictionary.save(temp_file)