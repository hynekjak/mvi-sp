from gensim.models.ldamulticore import LdaMulticore
from gensim.corpora.dictionary import Dictionary
from os import path

temp_file = path.join(path.dirname(path.abspath(__file__)), "model", "dictionary" )
common_dictionary = Dictionary.load(temp_file)

temp_file = path.join(path.dirname(path.abspath(__file__)), "model", "trained_model" )
lda = LdaMulticore.load(temp_file)

# vypis vsech temat a nejvyznacnejsich slov
for i in range(len(lda.get_topics())):
    first = True
    for j in lda.get_topic_terms(i):
        if first:
            print(common_dictionary[j[0]], end="")
            first = False
        else:
            print(", "+common_dictionary[j[0]], end="")
    print()