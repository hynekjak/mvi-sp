import sys
from gensim.models.ldamulticore import LdaMulticore
from gensim.corpora.dictionary import Dictionary
from os import path
import os
import json

if len(sys.argv) != 2:
    print('enter corpus folder path as argument')
    exit()

other_texts = list()

for root, dirs, files in os.walk(sys.argv[1]):  
    for filename in files:
        with open(os.path.join(root, filename), 'r') as wikiDump:
            for k in wikiDump:
                document = json.loads(k)
                other_texts.append(document['text'].split())

# nactu slovnik ze souboru
temp_file = path.join(path.dirname(path.abspath(__file__)), "model", "dictionary" )
common_dictionary = Dictionary.load(temp_file)

# nactu model ze souboru
temp_file = path.join(path.dirname(path.abspath(__file__)), "model", "trained_model" )
lda = LdaMulticore.load(temp_file)
print('model inicializovan')

other_corpus = [common_dictionary.doc2bow(text) for text in other_texts]

#_____________________________________________________

document_topics = list()
for i in range(len(other_corpus)):
    unseen_doc = other_corpus[i]
    vector = lda[unseen_doc] # get topic probability distribution for a document
    matching_topic = sorted(vector, key=lambda x: x[1], reverse=True)[0][0]
    document_topics.append(matching_topic)

for i in range(len(lda.get_topics())):
    with open('test_result/topic'+str(i)+'.txt', 'w') as a_file:
        for j in range(len(document_topics)):
            if document_topics[j] == i:
                for word in other_texts[j]:
                    a_file.write(word)
                    a_file.write(' ')
                a_file.write('\n\n')